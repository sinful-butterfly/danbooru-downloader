import urllib.request
import json
from bs4 import BeautifulSoup
import requests
import os
import shutil
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--tag")
parser.add_argument("--rating")


def download_stuff(tag, rating):
    url = None
    if rating == "safe" or rating == "sfw" or rating == "SFW":
        url = "https://danbooru.donmai.us/posts?ms=1&page=2&tags=" + tag + "+rating%3Asafe"
    else:
        url = "https://danbooru.donmai.us/posts?ms=1&page=2&tags=" + tag

    try:
        os.mkdir(tag)
        os.chdir(tag)
    except FileExistsError:
        os.chdir(tag)
    
    res = requests.get(url)
    html = res.text
    soup = BeautifulSoup(html, 'html.parser')
    pages_div = soup.findAll('li', attrs={'class': 'numbered-page'})
    pages = 0
    try:
        pages = pages_div[-1].text
    except IndexError:
        print("No results were found")
        exit(0)
    answer = input(pages + " were found, how many do you wish to download?(number/all)")
    pages_to_download = None
    if answer == "all" or answer == "" or answer == None:
        pages_to_download = pages
    
    else:
        pages_to_download = int(answer)
    
    print("Saving pics to: " + tag)
    for i in range(1, int(pages_to_download) + 1):
        api_url = None
        print("----------- PAGE " + str(i) + " out of " + pages  + " PAGES ----------")
        if rating == "safe":
            api_url = "https://danbooru.donmai.us/posts.json?ms=1&page=" + str(i) + "&tags=" + tag + "+rating%3Asafe"
        else:
            api_url = "https://danbooru.donmai.us/posts.json?ms=1&page=" + str(i) + "&tags=" + tag
            
        req = urllib.request.Request(api_url, headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' })
        r = urllib.request.urlopen(req).read()
        data = json.loads(r.decode('utf-8'))
        for x in data:
            try:
                file_url = x["file_url"]
                file_name = file_url.split("/")[-1]
                r = requests.get(file_url, stream=True, headers={'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'})
                if r.status_code == 200:
                    print("Downloading: " + file_name)
                    with open(file_name, 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                else:
                    print("Connection closed lol")
                
            except KeyError:
                print("This file has been removed")

tag = None
rating = "NSFW"

args = parser.parse_args()
if (args.tag == "" or args.tag == None):
    print("You need to specify a tag")
    exit(-1)
else:
    tag = args.tag

if (args.rating == "" or args.rating == None):
    print("No rating specified, defaulting to safe")
    rating = "safe"
else:
    rating = args.rating

download_stuff(tag, rating = rating)
